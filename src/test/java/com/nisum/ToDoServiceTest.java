package com.nisum;

import com.nisum.dao.ToDoDao;
import com.nisum.model.ToDo;
import com.nisum.service.ToDoService;

import junit.framework.Assert;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.anyLong;
 

@RunWith(MockitoJUnitRunner.class)
public class ToDoServiceTest {
	
	//clase a la cual se realizará mock
	@Mock
	private ToDoDao ToDoDao;
	
	@InjectMocks
	private ToDoService ToDoService; 
	

	@Test
	public void CrearToDoYRetornarlo() {
		
		//arrange
		ToDo ToDo = new ToDo(); 
		ToDo ToDoCreado = new ToDo();
		
		//act
		when(ToDoDao.save(ToDo)).thenReturn(ToDo);
		ToDoCreado = ToDoService.crearToDo(ToDo);
		
		//assert
		Assert.assertNotNull(ToDoCreado);
		Assert.assertEquals(ToDo,ToDoCreado);
	}
	
	@Test
	public void ObtenerToDoPorId() {
		
		//arrange
		ToDo ToDo = new ToDo();
		ToDo ToDoRetornado = new ToDo();
		
		//act
		when(ToDoDao.findById(anyLong())).thenReturn(ToDo);
		ToDoRetornado = ToDoService.obtenerToDoId(ToDo.getId());
		
		//assert
		Assert.assertNotNull(ToDoRetornado);
		Assert.assertEquals(ToDoRetornado, ToDo);
	}
	
	@Test
	public void ObtenerToDoPorCategoria() {
		
		//arrange
		ToDo ToDo = new ToDo();
		ToDo ToDoRetornado = new ToDo();
		
		//act
		when(ToDoDao.findByCategoria(toString())).thenReturn(ToDo);
		ToDoRetornado = ToDoService.obtenerToDoCategoria(ToDo.getCategoria());
		
		//assert
		Assert.assertNotNull(ToDoRetornado);
		Assert.assertEquals(ToDoRetornado, ToDo);
	}
	
	@Test
	public void ObtenerTodosLosToDo() {
		
		//arrange
		List<ToDo> misToDo = new ArrayList<ToDo>(); 
		List<ToDo> resultado = new ArrayList<ToDo>();
		
		//act
		when(ToDoDao.findAll()).thenReturn(misToDo);
		resultado = ToDoService.obtenerTodos();
		
		//assert
		Assert.assertNotNull(resultado);
		Assert.assertEquals(misToDo, resultado);
	}
}

