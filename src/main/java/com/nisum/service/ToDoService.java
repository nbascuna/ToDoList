package com.nisum.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.nisum.dao.ToDoDao;
import com.nisum.model.ToDo;

public class ToDoService {
	//inyecci�n clase
		@Autowired
		private  ToDoDao ToDoDao;
		
		public  ToDo crearToDo(ToDo ToDo) {
			return ToDoDao.save(ToDo);
		}
		
		public ToDo obtenerToDoId(long id) {
			// TODO Auto-generated method stub
			ToDo ToDo = new ToDo();
			ToDo = ToDoDao.findById(id);
			return ToDo;
		}
		
		public ToDo obtenerToDoCategoria(String categoria) {
			// TODO Auto-generated method stub
			ToDo ToDo = new ToDo();
			ToDo = ToDoDao.findByCategoria(categoria);
			return ToDo;
		}

		public List<ToDo> obtenerTodos() {
			// TODO Auto-generated method stub
			List<ToDo> misToDo = new ArrayList<ToDo>();
			misToDo = (ArrayList<ToDo>) ToDoDao.findAll();
			return misToDo;
	}
}
