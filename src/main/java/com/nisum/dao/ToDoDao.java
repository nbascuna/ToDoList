package com.nisum.dao;

import org.springframework.data.repository.CrudRepository;

import com.nisum.model.ToDo;
import com.nisum.service.*;

public interface ToDoDao extends CrudRepository<ToDo, Long> {
	public ToDo findById(long id);
	public ToDo findByCategoria(String categoria);
}

